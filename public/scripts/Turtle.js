export default function TurtleRenders({
    instructions = "F",
    distance = 10,
    rotation = 90,
    coords = {x: 0, y: 0},
    orientation = 0,
    commands = {}
  }){
    
    const commandList = instructions.split("")
      
    const renders = []
  
    commandList.forEach(command => {
      const key = command
      const result = commands[command]({key, instructions, distance, rotation, coords, orientation, commands})
      if(!result) return
      if(result.data) renders.push(result.data)
      if(result.coords) coords = result.coords
      if(result.orientation !== undefined) orientation = result.orientation
    })
  
    return renders
     
  }

export function moveTurtle(key, coords, distance, orientation){
    const start = coords
    const end = calculateNewLocation(start, distance, orientation)

    const result = {
        coords: end
    }

    if(key === "F") result.data = {start, end}

    return result
}
  
export function calculateOrientation(orientation, set){
    let newRotation = orientation + set
    if(newRotation >= 360) newRotation = 360 - newRotation 
    if(newRotation < 0) newRotation = 360 + newRotation
    return newRotation
}
  
function calculateNewLocation(start, distance, orientation){
    const x = distance * parseFloat(angleToRadian("cos", orientation).toFixed(3)) + start.x
    const y = distance * parseFloat(angleToRadian("sin", orientation).toFixed(3)) + start.y

    return {x,y}
}

export function calculateRotationFromPoints(start, end){
    return Math.atan2(end.y - start.y, end.x - start.x) * 180 / Math.PI
}
  
function angleToRadian(method, a){
    return Math[method](Math.PI * a / 180)
}
