/**
 *  MIT License

    Copyright (c) 2020 Future Total Group

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
 */

 /**
  *     Welcome to Future Total Labs, an outreach branch of the Future Total Group.
  *     I am Jake, a TO_BE_DECIDED with the Future Total Group. Previously myself
  *     and cohort Jacob Canyon Robinson have hosted public events for FTG, but,
  *     due to the pandemic, we've all been forced indoors. 
  * 
  *     Future Total Labs is our effort to bring the wonderful research being done
  *     at the FTG to the public in an informative and, hopefully, entertaining light.
  *     We'll be releasing more information about this effort on our instagram
  *     @futuretotalgroup
  */


 /**
  *     Today we will be discussing L-Systems, a fundamental concept within the
  *     field of Algorithmic Botany. My presentation is largely based on the work
  *     presented on http://algorithmicbotany.org/ by Professor Przemyslaw
  *     Prusinkiewicz and the University of Calgary. I, however have no
  *     affiliation with Professor Prusinkiewicz or the University of Calgary and
  *     act solely as an agent of the Future Total Group.
  * 
  *     So, without further ado, let's get into it. 
  * 
  * =============================================================================
  * 
  *     ALGORITHMIC BOTANY
  * 
  *     Algorithmic Botany is the study of botanical life and the recreation
  *     through algorithmic means. In English this basically means having a garden
  *     on your computer.
  * 
  *     Over the years techniques have emerged for simulating plants. Today we're
  *     going to start with the very basic Lindenmayer system, or L-systems
  * 
  */

import LSystem from "./LSystem.js"
import updateUI from "./index.js"

/*
    What the L is an L-System?
    ================================================================================
    Aristid Lindenmayer was a Hungarian biologist studying the growth of various algae.
    In these studies, Lindenmayer conceived of a Formal Language for describing the 
    growth of simple organisms like this, known as Lindenmayer Systems or the much 
    more convenient L-system.

    Lindenmayer systems are Formal Languages consisting of an alphabet (V) with 
    which words (w) can be formed and a set of Production (P) rules for altering the 
    strings.
*/

//  So, a very simple example of this would be the following:

//  An alphabet consisting of characters "a" and "b"
const V = [ "a", "b" ]

/**
 *  Here we have a list of production rules:
 *      a => ab
 *      b => a
 * 
 *  Meaning that any instance of "a" within our word w, will be transformed into
 *  "ab", while any instance of character "b" will become "a"
 */
const P = [
    { key: "a", production: "ab"},  // a => ab
    { key: "b", production: "a"}    // b => a
]

//  This is the word we will run the Production Rules on
const w = "a"

/**
 *  This is the number of times the Production Rules will be run on the word w.
 *  The output of each run is fed in as the input of the next run. We'll look
 *  into this a little bit more later
 */
const n = 3

const output = LSystem({V, n, w, P})

updateUI(w, output)

/**
 *  This L-system was used to describe the multicellular filament, like the ones
 *  found in blue-green bacteria 'Anabaena catenula' and various other algae.
 * 
 *  Each of these letters is said to represent the state of the cell with
 *  relations to its size and its readiness to divide. 'a' can be viewed as a
 *  cell that is ready to divide and 'b' can be seen as a cell that is still
 *  growing
 * 
 *  We could go into details about cell polarity and designating a left and right
 *  polarity, but this will make more sense later.
 * 
 *  It's this ability to represent the state of an organism with a simple alphabet
 *  that gives L-systems such incredible power.
 */