export default function LSystem({
    V = [],
    n = 1,
    w = "F",
    P = []
  }){
    const obj = {}
    
    P.forEach(p => {
      obj[p.key] = p.production
    })
    
    P = obj
    
    let i = 0
    let output = w
    let stages = [w]
    while(i < n){
      let setString = ""
      output.split("").forEach(char => {
        if(P[char]){
          setString += P[char]
        }else{
          setString += char
        }
      })
      stages.push(setString)
      output = setString
      i++
    }
  
    return {output, stages}
}