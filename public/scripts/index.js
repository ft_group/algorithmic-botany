export default function updateUI(w, output){
    inputEl.innerHTML = `input: ${w}`
    output.stages.forEach((stage, i) => {
        const stageEl = createStageElement(`output ${i}: ${stage}`)
        stagesEl.appendChild(stageEl)
    })
    outputEl.innerHTML = `result: ${output.output}`
}

const inputEl = document.getElementById("input")
const stagesEl = document.getElementById("stages")
const outputEl = document.getElementById("output")

function createStageElement(text){
    const el = document.createElement("div")
    el.classList.add("stage")
    el.innerHTML = text
    return el
}