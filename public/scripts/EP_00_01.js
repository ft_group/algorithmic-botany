/**
 *  MIT License

    Copyright (c) 2020 Future Total Group

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
 */
import LSystem from "./LSystem.js"
import TurtleRenders, {moveTurtle, calculateOrientation, calculateRotationFromPoints} from "./Turtle.js"
import config from "./turtleConfig.js"
import updateUI from "./index.js"
 
 /**
  *     Alright. So, what? We can make strings into other strings. Big deal!
  *     Big deal, indeed! Next let's talk about Turtles!
  * 
  *     Yes, Turtles!
  * 
  *     Alright, let's say we have a robot turtle. Let's call him A'Tuin.
  *     A'Tuin is just chilling on a sandy beach on standby waiting for us to
  *     command him around. And we're gunna command him.
  * 
  *     A'Tuin can take four commands:
  *         *   Move forward in the direction you're facing and drag your 
  *             flipper(?) so that it makes a line in the sand. We've got a turtle 
  *             with good boundaries, also known as "edges", but we'll touch on 
  *             that later.
  *         *   Move forward in the direction you're facing, but don't drag your
  *             flipper so that your movements are invisible
  *         *   Rotate clockwise a predetermined number of degrees
  *         *   Rotate counterclockwise a predetermined number of degrees
  * 
  *     Cool. We've got an obedient turtle, but now we need some interesting 
  *     commands to send him. 
  * 
  *     If only we had a simple way of packaging commands...
  * 
  *     Well we do. Turns out, Characters in Formal Languages aren't just good for
  *     representing states. They're also great for representing commands. Ah! And
  *     now it all comes together.
  * 
  *     I'm just kidding. These are still just building blocks. But let's get our
  *     boy A'Tuin off to work.
  * 
  *     So, first we'll need to encode our commands into characters of an
  *     alphabet. Here goes:
  *     
  *     F : Move forward and draw
  *     f : Move forward without drawing
  *     + : Rotate a predefined amount clockwise
  *     - : Rotate a predefined amount counter clockwise
  */
 
 const A = [ "F", "f", "+", "-" ]
 
 const P = [
     { key: "F", production: "F-fF-F-f"},
 ]
 
 //  This is the word we will run the Production Rules on
 const w = "F-F-F-F"
 
 /**
  *  This is the number of times the Production Rules will be run on the word w.
  *  The output of each run is fed in as the input of the next run. We'll look
  *  into this a little bit more later
  */
 const n = 5
 
 const output = LSystem({A, n, w, P})
 
 updateUI(w, output)

 /**
  *     Alright, now we need to feed the string we output from the L-system in
  *     as commands to our turtle. 
  * 
  *     Oh! And we'll need to configure the Turtle!
  *     We need to set his starting coordinates, his starting orientation, the
  *     distance he should travel with each travel command and the amount he
  *     should rotate with each rotation command. You'll see that below, along
  *     with some other necessary JavaScript nonsense. You only have to worry
  *     about that if you're a NERD!
  */

 let Turtle = TurtleRenders(
   Object.assign(config,{      
    instructions: output.output, //Inputting the string output by the LSystem
    commands: {
       "F": ({key, coords, distance, orientation}) => moveTurtle(key, coords, distance, orientation),
       "f": ({key, coords, distance, orientation}) => moveTurtle(key, coords, distance, orientation),
       "-": ({rotation, orientation}) => {
         return { orientation: calculateOrientation(orientation, rotation * -1) }
       },
       "+": ({rotation, orientation}) => {
         return { orientation: calculateOrientation(orientation, rotation) }
       }
    }
  })
)




















const canvasConfig = {x: 875, y: 650}
const xRange = getEdgeRange("x", Turtle)
const yRange = getEdgeRange("y", Turtle)
const xDelta = xRange.reduce((acc, x) => acc - x)
const yDelta = yRange.reduce((acc, x) => acc - x)

const canvasMid = {}
Object.keys(canvasConfig).forEach(key => {
  canvasMid[key] = canvasConfig[key]/2
})

console.log(canvasMid.x + (xDelta/2))

const xSet = (canvasMid.x + (xDelta/2)) - xRange[0]
const ySet = (canvasMid.y + (yDelta/2)) - yRange[0]

Turtle = Turtle.map(e => {
  return { 
    start: {
      x: e.start.x + xSet,
      y: e.start.y + ySet
    },
    end: {
      x: e.end.x + xSet,
      y: e.end.y + ySet
    }
  }
})
const renderedEdges = []
const renderSpeed = 10
let renderDistance = renderSpeed
let renderIndex = 0

const sketch = sk => {
    sk.setup = () => {
        sk.createCanvas(canvasConfig.x, canvasConfig.y)
    }

    sk.draw = () => {
      sk.fill("#bababa")
      sk.rect(0,0,1000,1000)
      sk.stroke("#ff0000")

      const getTurtle = Turtle[renderIndex]
      if(getTurtle && renderDistance >= config.distance){
        renderedEdges.push(getTurtle)
        renderIndex++
        renderDistance = renderSpeed
      }else if(getTurtle){
        const rotation = calculateRotationFromPoints(getTurtle.start, getTurtle.end)
        const drawLine = moveTurtle("F", getTurtle.start, renderDistance, rotation).data 
        sk.line(drawLine.start.x, drawLine.start.y, drawLine.end.x, drawLine.end.y)
        renderDistance += renderSpeed
      }else{
        sk.stroke("#0000ff")
      }
      
      renderedEdges.forEach(e => {
        sk.line(e.start.x, e.start.y, e.end.x, e.end.y)
      })
    }
}

const P5 = new p5(sketch)

function getEdgeRange(dim, list){
  return list
  .map(e => [e.start[dim], e.end[dim]])
  .flat()
  .sort((a, b) => b - a)
  .filter((e, i, arr) => i === 0 || i === arr.length - 1)
}